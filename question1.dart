void talk(var name, var favouriteApp, var city) {
  print("\nHi my name is $name");
  print("My favourite App is $favouriteApp");
  print("My city name is $city\n");
}

void main() {
  var name = 'bouy';
  var favouriteApp = 'Ambani Africa';
  var city = 'Johannesburg';

  talk(name, favouriteApp, city);
}
