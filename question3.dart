void main(List<String> args) {
  BusinessApp app1 =
      BusinessApp("FNB Team", "Best Financial Solution", "FNB", 2012);
  BusinessApp app2 =
      BusinessApp("SnapScan Team", "Best HTML 5 Solution", "SnapScan", 2013);
  BusinessApp app3 = BusinessApp("LIVE Inspect Team",
      "Best Android Solution (Enterprise)", "LIVE Inspect", 2014);
  BusinessApp app4 =
      BusinessApp("WumDrop Team", "Best Enterprise Solution", "WumDrop", 2015);
  BusinessApp app5 =
      BusinessApp("Domestly Team", "Best Consumer Solution", "Domestly", 2016);
  BusinessApp app6 =
      BusinessApp("Shyft Team", "Best Financial Solution", "Shyft", 2017);
  BusinessApp app7 = BusinessApp("Khula ecosystem Team",
      "Best Agriculture Solution", "Khula ecosystem", 2018);
  BusinessApp app8 = BusinessApp("Naked Insurance Team",
      "Best Financial Solution", "Naked Insurance", 2019);
  BusinessApp app9 = BusinessApp(
      "EasyEquities Team", "Best Consumer Solution", "EasyEquities", 2020);
  BusinessApp app10 = BusinessApp(
      "Ambani Africa Team", "Best Educational Solution", "Ambani Africa", 2021);

  var appList = [];
  appList.add(app1);
  appList.add(app2);
  appList.add(app3);
  appList.add(app4);
  appList.add(app5);
  appList.add(app6);
  appList.add(app7);
  appList.add(app8);
  appList.add(app9);
  appList.add(app10);

  print(
      "|      APP NAME      |     SECTOR        |       DEVELOPER       |       YEAR      |");
  print(
      "|----------------------------------------------------------------------------------|");

  for (var i = 0; i <= 9; i++) {
    print(
        "|${appList[i].name}    ${appList[i].sector}    ${appList[i].developer}    ${appList[i].year}  |");
  }

  print("");
  print("|--------------------|");
  print("|      APP NAME      |");
  print("|--------------------|");
  for (var x = 0; x <= 9; x++) {
    print("|${app1.upperCase(appList[x].name)} ");
  }
}

class BusinessApp {
  String name;
  String sector;
  String developer;
  int year;

  BusinessApp(this.developer, this.sector, this.name, this.year) {
    name = upperCase(name);
  }

  String upperCase(String value) {
    return value.toUpperCase();
  }
}
