void appWinners(var appWinners, var app17, var app18) {
  // sort and print the apps by name
  print(appWinners);

  // print the winning app of 2017 and 2018
  print("\nWinning app: ${app17[7]}");
  print("Winning app: ${app18[4]}");
}

void main() {
  List appWinnersList = [
    "FNB-2012",
    "SnapScan-2013",
    "LIVE Inspect-2014",
    "WumDrop-2015",
    "Domestly-2016",
    "Shyft-2017",
    "Khula ecosystem-2018",
    "Naked Insurance-2019",
    "EasyEquities-2020",
    "Ambani Africa-2021"
  ];

  appWinnersList.sort();
  appWinners(appWinnersList, appWinnersList, appWinnersList);

  // print the total number of apps
  print(
      "\nThe total number of apps in the list is: ${appWinnersList.length}\n");
}
